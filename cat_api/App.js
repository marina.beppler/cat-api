import{ NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import HomeScreen from './src/screens/HomeScreen';
import Favorites from './src/screens/Favorites';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name="Home" options={{headerStyle:styles.header}} component={HomeScreen} />
      <Stack.Screen name="Favorites" options={{headerStyle:styles.header}} component={Favorites} />
    </Stack.Navigator>
  </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#B8BEDD',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    backgroundColor: '#51A3A3',
  },
});
