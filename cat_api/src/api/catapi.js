import axios from 'axios';
import { API_KEY } from '@env';

export default axios.create({
    baseURL: "https://api.thecatapi.com/v1",
    params: {
        api_key: API_KEY
    }
});

