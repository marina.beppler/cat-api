import React, { useEffect, useState } from "react";
import { View, Text, Button, StyleSheet } from 'react-native';
import { AntDesign } from '@expo/vector-icons'; 
import catapi from "../api/catapi";

const HomeScreen = ({navigation, route}) => {
    const [catData, setCatData] = useState (null);
    const [catFavorite, setFavorite] = useState(null);

    useEffect(() => {
      getCat(route.params);
    }, []);
      
    async function getCat() {
      try {
        const response = await catapi.get(`/images/search`, {
          params: {
            limit: 25,
            has_breeds: true
          }
        })
        console.log(response.data);
        setCatData(response.data);
      }
      catch (err) {
        console.log(err);
      }
  
    }

    async function postFavorites(id)  {
      try {
        const response = await catapi.post(
          `/favourites`, 
          {
            image_id: id,
            sub_id: 'teste01'
          }
        );
        setFavorite(response.data);
      }
      catch (err) {
        console.log(err);
      }
  
    }

    return (
      <View style={styles.container}>
          <Text style={styles.title}>CAT IMAGE GENERATOR</Text>
          <Text style={styles.normalFont}>
            Para gerar imagens aleatórias de gatos, clique no botão "Randomizar" abaixo.
            Para favoritar uma imagem, clique no botão de coração. 
            Para acessar os favoritos, clique no botão "Favoritos" abaixo
          </Text>
          <div id="grid" className="imgrid" style={styles.imageGrid}>
            {catData?.slice(0,4).map((cat) => (
              <div className="image-button-pair" key={cat.id}>
                <Text style={styles.subtitle}>{cat.breeds[0].name}</Text>
                <img style={styles.img} className="grid-image" src={cat.url}/>
                <button className="grid-button" style={styles.button} onClick={() => postFavorites(cat.id)}>
                  <AntDesign name="hearto" size={24} color="black" />
                </button>
              </div>
            ))}
          </div>
          <Button
              title="Randomizar"
              onPress={() => getCat()}
          />
          <Button
              title="Favoritos"
              onPress={() => navigation.navigate('Favorites')}
          />
      </View>
  );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#D8DCFF',
        textAlign: 'center',
        color: 'black',
        fontSize: 20,
    },
    button:{
      backgroundColor: '#EF5B5B',
      color: '#fff',
      border: 0,
      display: 'block',
      width: '100%',
      marginBottom: 10
    },
    title: {
      fontSize: 40,
      fontFamily: 'sans-serif-thin',
      color: '#51A3A3',
      fontWeight: '600'
    },
    normalFont: {
      fontWeight: '200',
      color: '#000500'
    },
    imageGrid:{
      display: 'flex',
      flexDirection: 'column'

    },
    img:{
      maxWidth: '100%'
    },
    subtitle:{
      fontFamily: 'sans-serif-thin',
      fontWeight: '400',
      color: '#565676',
      fontSize: 30
    }
})
export default HomeScreen;