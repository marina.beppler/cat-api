import React, {useState, useEffect} from "react";
import { View, Text, StyleSheet, Button } from 'react-native';
import catapi from "../api/catapi";
 
const Favorites = ({route}) => {
  const [fav, setFav] = useState(null);
  const [unfav, setUnfav] = useState(null);

  useEffect(() => {
    getFavorite(route.params);
  }, []);
  
  async function getFavorite() {
    try {
      const response = await catapi.get(`/favourites`, {
        params: {
          sub_id: 'teste01',
        }
      });
      setFav(response.data);
    }
    catch (err) {
      console.log(err);
    }
  }

  return(
    <View style={styles.container}>
        <Text style={styles.title}>Favoritos</Text>
        <div id="grid" className="imgrid" style={styles.imageGrid}>
          {fav?.map((favo) => (
              <div key={favo.id}>
                <img style={styles.img} src={favo.image.url}/>
              </div>
          ))}
        </div>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#D8DCFF',
        textAlign: 'center',
        color: 'black',
        fontSize: 20,
    },
    button:{
      backgroundColor: '#EF5B5B',
      color: '#fff',
      border: 0,
      display: 'block',
      width: '100%',
      marginBottom: 10
    },
    title: {
      fontSize: 40,
      fontFamily: 'sans-serif-thin',
      color: '#51A3A3',
      fontWeight: '600'
    },
    normalFont: {
      fontWeight: '200',
      color: '#000500'
    },
    imageGrid:{
      display: 'flex',
      flexDirection: 'column'

    },
    img:{
      maxWidth: '100%'

    }
})

export default Favorites;